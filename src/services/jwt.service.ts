import { encode, decode } from 'jwt-simple';
import moment from 'moment';
import { env } from '../config/env';

export class JWTService {
    static createToken(data: any) {
        const token = encode(data, env.JWT_KEY);
        return token;
    }
    static decodeToken(token: string) {
        let payload: any = {};
        try {
            payload = decode(token, env.JWT_KEY);
            if (payload.exp < moment().unix()) return { success: false, status: 401, message: 'Token Expirado' };
        } catch (e) {
            return { success: false, status: 403, message: 'Token No Valido' };
        }
        return { success: true, payload };
    }
}
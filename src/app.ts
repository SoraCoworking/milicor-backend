import express from 'express';
import morgan from 'morgan';
import cors from 'cors';

// cargar rutas 
import { userRoutes } from './routes/user.routes';
import { categoryRoutes } from './routes/category.routes';
import { productRoutes } from './routes/product.routes';
import { commerceRoutes } from './routes/commerce.routes';
import { commerceProductRoutes } from './routes/commerce-product.routes';
import { DASHBOARD_ROUTES } from './routes/dashboard.routes';

// midlewares
export const app = express();
app.use(morgan('dev'));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());

// rutas
app.use('/users', userRoutes);
app.use('/categories', categoryRoutes);
app.use('/products', productRoutes);
app.use('/commerces', commerceRoutes);
app.use('/commerce-products', commerceProductRoutes);
app.use('/dashboard', DASHBOARD_ROUTES);
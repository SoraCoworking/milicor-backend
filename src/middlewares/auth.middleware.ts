import { NextFunction, Response } from "express";
import { JWTService } from "../services/jwt.service";

export function Auth(req: any, res: Response, next: NextFunction) {
    const { authorization: token } = req.headers;
    const response = JWTService.decodeToken(req);
    if (!response.success) return res.status(response.status).send({ errors: response.message });
    req.user = response.payload;
    next();
}
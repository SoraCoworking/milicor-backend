import { model, Schema } from "mongoose";

const ProductsSchema = new Schema({
    name: { type: String, required: true},
    description: { type: String, required: true},
    content: { type: String, required: true},
    category: { type: Schema.Types.ObjectId, ref: "Category", required: true},
    image: { type: String, required: true},
    marca: { type: String, required: false },
});

export const Product = model('Product', ProductsSchema);
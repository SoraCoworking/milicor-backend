import { model, Schema } from "mongoose";

const CategorySchema = new Schema({ 
    name: { type: String, required: true },
    image: { type: String, required: true },
    order: {type: Number, required: true }, 
});

export const Category = model('Category', CategorySchema);
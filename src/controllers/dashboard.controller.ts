import { Category } from "../models/category.model";
import { Product } from "../models/product.model";

export class DashboardController {
    async getProductsOrCategories(req, res) {
        const filter = req.params.filter;
        console.log(filter);
        const filterObj = { name: { $regex: `.*${filter}.*`, $options: 'i' } };
        // get products
        const products = await Product.find(filterObj);
        // get Categories
        const categories = await Category.find(filterObj);

        return res.status(200).send({ products, categories });
    }
}
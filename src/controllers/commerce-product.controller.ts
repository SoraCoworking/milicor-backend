import { Request, Response } from "express";
import { CommerceProduct } from "../models/commerce-product.model";
import { formValidation } from "../services/form-validation.service";

export class CommerceProductController {
    async show(req: Request, res: Response) {
        const { _id } = req.params;
        const commerceProduct = await CommerceProduct.findById(_id).populate('commerce product');
        if (!commerceProduct) return res.status(404).send({ message: 'No existe el producto en el commercio'});
        return res.status(200).send(commerceProduct);
    }
    async getByProduct(req: Request, res: Response) {
        const { product } = req.params;
        return res.status(200).send(await CommerceProduct.find({ product }).populate('commerce'));
    }
    async store(req: Request, res: Response) {
        const { body } = req;
        const validForm = await formValidation.validateForm(body, {
            product: 'required|string',
            commerce: 'required|string',
            price: 'required|numeric|min:1'
        });
        if (!validForm.success) return res.status(400).send({ message: 'Formulario Invalido', ...validForm });
        const commerceProduct = await (new CommerceProduct(body)).save();
        return res.status(200).send(commerceProduct);
    }
}
import { Request, Response } from "express";
import { Category } from "../models/category.model";
import { formValidation } from "../services/form-validation.service";

export class CategoryController {
    async show(req: Request, res: Response) {
        const { _id } = req.params;
        const category = await Category.findById(_id);
        if (!category) return res.status(404).send({ message: 'Categoría no encontrada' });
        return res.status(200).send(category);
    }

    async index(req: Request, res: Response) {
        return res.status(200).send(await Category.find().sort('order'))
    }

    async store(req: Request, res: Response) {
        const { body } = req;
        const validForm = await formValidation.validateForm(body, {
            name: 'required|string|minLength:3',
            image: 'required|string',
            order: 'required|numeric',
        });
        if (!validForm.success) return res.status(400).send({ message: 'Formulario Invalido', ...validForm });
        const category = await (new Category(body)).save();
        return res.status(200).send(category);
    }   
}
import { Request, Response } from "express";
import moment from "moment";
import { User } from "../models/user.model";
import { formValidation } from "../services/form-validation.service";
import { JWTService } from "../services/jwt.service";

export class UserController {
    async register (req: Request, res: Response) {
        const { body } = req
        const formValid = await formValidation.validateForm(body, {
            email: 'required|email',
            name: 'required|minLength:3',
            phone_number: 'required|numeric|min:10'
        });
        if (!formValid.success) return res.status(400).send({ message: 'Formulario inválido', ...formValid });
        // validar si existencia
        const userExists = await User.findOne({ body })
        let user; 
        if (userExists) {
            user = userExists;
        } else {
            user = await (new User(body)).save();
        }
        // crear token
        const token = JWTService.createToken({
            sub: user._id,
            name: user.name,
            email: user.email,
            phone_number: user.phone_number,
            iat: moment().unix(),
            exp: moment().add('2', 'months').unix()
        });
        res.status(200).send({user, token });
    }
}
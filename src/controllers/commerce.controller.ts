import { Request, Response } from "express";
import { Commerce } from "../models/comercio.model";
import { formValidation } from "../services/form-validation.service";
export class CommerceController {
    async store(req: Request, res: Response) {
        const { body } = req;
        const validForm = await formValidation.validateForm(body, {
            name: 'required|minLength:3',
            address: 'required|string|minLength:3',
            phone: 'required|string',
            image: 'required|string',
        }); 
        if (!validForm.success) return res.status(400).send({ message: 'Formulario Invalido', ...validForm })
        const commerce = await (new Commerce(body)).save();
        return res.status(200).send(commerce);
    }
}
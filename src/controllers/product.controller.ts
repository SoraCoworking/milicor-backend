import { Request, Response } from "express";
import { Product } from "../models/product.model";
import { formValidation } from "../services/form-validation.service";

export class ProductController {
    async show(req: Request, res: Response) {
        const { _id } = req.params;
        const product = await Product.findById(_id);
        if (!product) return res.status(404).send({ message: 'Prducto no existe' });
        return res.status(200).send(product);
    }
    async index(req: Request, res: Response) {
        const { category } = req.params;
        const { marca } = req.query;
        const filter = { category };
        if (marca) filter['marca'] = { $regex: `.*${marca}.*`, $options: 'i' };
        return res.status(200).send(await Product.find(filter));
    }
    async store(req: Request, res: Response) {
        const { body } = req;
        const validForm = await formValidation.validateForm(body, {
            name: 'required|string',
            description: 'required|string',
            content: 'required|string',
            category: 'required|string',
            image: 'required|string'
        });
        if (!validForm.success) return res.status(400).send({ message: 'Formulario Invalido', ...validForm });
        const product = await (new Product(body)).save();
        return res.status(200).send(product);
    }
    async getVariant (req, res) {
        const filter = req.params.filter;
        console.log(filter);
        const filterObj = { name: { $regex: `.*${filter}.*`, $options: 'i' } };
        // get products
        const products = await Product.find(filterObj);
        return res.status(200).send(products);
    } 
    
}
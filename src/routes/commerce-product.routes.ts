import { Router } from "express";
import { CommerceProductController } from "../controllers/commerce-product.controller";

const commerceProductController = new CommerceProductController();

export const commerceProductRoutes = Router();

commerceProductRoutes.get('/:_id', commerceProductController.show);
commerceProductRoutes.get('/product/:product', commerceProductController.getByProduct);
commerceProductRoutes.post('/', commerceProductController.store);

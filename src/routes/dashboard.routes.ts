import { Router } from "express";
import { DashboardController } from "../controllers/dashboard.controller";

const dashboardController = new DashboardController();

export const DASHBOARD_ROUTES = Router();

DASHBOARD_ROUTES.get('/products-categories/filter/:filter', dashboardController.getProductsOrCategories);


import { Router } from "express";
import { ProductController } from "../controllers/product.controller";

const productController = new ProductController();

export const productRoutes = Router();

productRoutes.get('/category/:category', productController.index);
productRoutes.get('/:_id', productController.show);
productRoutes.get('/filter/:filter', productController.getVariant);
productRoutes.post('/',productController.store);

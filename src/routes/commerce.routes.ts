import { Router } from "express";
import { CommerceController } from "../controllers/commerce.controller";

const commerceController = new CommerceController();

export const commerceRoutes = Router();
commerceRoutes.post('/', commerceController.store);
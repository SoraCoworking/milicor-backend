import { Router } from "express";
import { CategoryController } from "../controllers/category.controllers";

const categoryController = new CategoryController();

export const categoryRoutes = Router();

categoryRoutes.get('/', categoryController.index);
categoryRoutes.get('/:_id', categoryController.show);
categoryRoutes.post('/', categoryController.store);
